# README #

SimpleVeggieGarden is a bash script I wrote to draw my garden in SVG.
The history of the script and challenges I encountered are reviewed on [My website](https://www.aroundtheglobe.biz/posts/20211005-SimpleVeggieGarden_-_roadblocks_on_a_simple_gps2svg_script.html) - Recommended read!
This is still an alpha version: some functions are incomplete/missing/barely tested/undocumented

### What is this repository for? ###

* Setup a taxon database (or use one of the included ones)
* Manage different projects (plots)
* Input GPS coordinates for a poly/taxon in DB - and map them in an SVG file
* UI in french/english/spanish, plantnames in latin + (french/english where it was available) - I should add Spanish plantnames, but am still looking for a decent db to import the names from.
* Minimal and modular code, easily extendable for other projects/languages
* BSD licence: feel free to use, but please mention where you got the source!

### Dependencies ###

* mariadb/mysql
* bc

### Files: ###
* svg.sh : The plot management/drawing part.
* taxondb.sh : what I used to generate my taxon db
* commonconf.settings.sh : common scripts and config between svg.sh and taxondb.sh

### How do I get set up? ###

* Install dependencies
* Create db and user, set rights.
* Set credentials in commonconf.settings.sh
* Import taxon DB with mysql or create your own with taxondb first
* ./svg.sh
- Create project
- Add poly
- Add trees
- Render svg

### Usage ###

* ./svg.sh
Then it was kind of an attempt to make menus self explanatory
Basically, you create a project, and then want to edit it.
Once you are in the project, you can add trees or poly, trees is any kind of plant, while poly stands for polypoint.
A poly is either a polygon or a path.
A path is anything you could draw with an open line: path, river, power/wate/gas/... line, ...
A polygon is anything you could draw with a closed line: parcel, cultivation zone, house, pool, lake ...

* ./taxondb.sh
If you want to generate your own taxon DB, you will need to edit it anyway!
I havn't used it in a few months since my current db is fine for me so far!

### DB structure ###

The taxon database is the backbone of SimpleVeggieGarden.  Current version includes most living beings from France, and most plants from Canada.
If you live in another region, you might hace to compile your own.

root@localhost [SimpleVeggieGarden]> show tables;
+------------------------------+
| Tables_in_SimpleVeggieGarden |
+------------------------------+
| UI                           | --> UI table contains string translations for UI
| project1_cultures            | --> culture table for project1
| project1_events              | --> events table for project1
| project1_poly                | --> poly table for project1
| project1_relations           | --> relation table for project1
| project1_trees               | --> trees table for project1
| specs_habitats               | --> referenced by taxons
| specs_rangs                  | --> referenced by taxons
| specs_relations              | --> referenced by taxons
| taxon_Animalia               | --> Animals from France
| taxon_Archaea                | --> Archaea from France
| taxon_Bacteria               | --> Bacteria from France
| taxon_Chromista              | --> Chromista from France
| taxon_Fungi                  | --> Fungi from France
| taxon_Plantae                | --> Plants from France and Canada
| taxon_Protozoa               | --> Protozoa from France
| tempmerge                    | --> Used as temp table to merge canadian plants
| test_cultures                | --> culture table for test
| test_events                  | --> events table for test
| test_poly                    | --> poly table for test
| test_relations               | --> relation table for test
| test_trees                   | --> trees table for test
+------------------------------+

I'll try to put more documentation one day!
