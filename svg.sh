#!/usr/bin/env bash
## #!/bin/bash
#version 030
pproject="$1"
source commonconf.settings.sh
curmonth="$(date +%m)"
# CORE functions: sufficient for hardcoded single project without UI and inputting data by hand in sql console
function f_size_scale { # define map size and scale - Inspired by https://adventofcode.com and https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
	query="select min(lattit),min(longit) from ${pproject}_trees"
	f_query && mintree="$(echo "${sqlanswer[@]}"| sed -e 's/\t/,/g' -e 's/ /,/')"
	query="select max(lattit),max(longit) from ${pproject}_trees"
	f_query && maxtree="$(echo "${sqlanswer[@]}"| sed -e 's/\t/,/g' -e 's/ /,/')"
	latts=(); longs=()
	query="select coordinates from ${pproject}_poly"
	f_query2
	results+=("$mintree" "$maxtree")
	minlattit="0"; maxlattit="0"; minlongit="0"; maxlongit="0"
	for i in "${results[@]}"; do #define max/min lat/lon
		lat=$(echo "$i"|cut -f1 -d',') ; lon=$(echo "$i"|cut -f2 -d',')
		if [[ "$minlattit" = "0" ]]; then
			minlattit=$lat;maxlattit=$lat; minlongit=$lon;maxlongit=$lon
		else
			num1="$lat";num2="$maxlattit"; if [ "$(echo "$num1>$num2"|bc -l)" -gt 0 ]; then maxlattit="$lat"; fi
			num1="$lat";num2="$minlattit"; if [ "$(echo "$num1<$num2"|bc -l)" -gt 0 ]; then minlattit="$lat"; fi
			num1="$lon";num2="$maxlongit"; if [ "$(echo "$num1>$num2"|bc -l)" -gt 0 ]; then maxlongit="$lon"; fi
			num1="$lon";num2="$minlongit"; if [ "$(echo "$num1<$num2"|bc -l)" -gt 0 ]; then minlongit="$lon"; fi
		fi
		latts+=(" $lat"); longs+=(" $lon")
	done
	avglattit=$(echo "${latts[@]}" | awk '{ total += $2; count++ } END {printf "%.15g\n", total/count }')
	deltalat=$(echo "define vabs(x) {if (x<0) {return -x}; return x;}; vabs($maxlattit - $minlattit)"|bc -l)
	latmid=$(echo "define vabs(x) {if (x<0) {return -x}; return x;}; vabs(4*a(1) * $avglattit / 180)"|bc -l)
	metreperlattit=$(echo "111132.954 - 559.822 * c( 2 * $latmid ) + 1.175 * c( 4 * $latmid)"|bc -l)
	landsizelat=$(echo "scale=2; ($metreperlattit * $deltalat) / 1"|bc)
	deltalon=$(echo "define vabs(x) {if (x<0) {return -x}; return x;}; vabs( $maxlongit - $minlongit )"|bc -l)
	mplongit=$(echo "111132.954*c( $latmid )"|bc -l)
	landsizelon=$(echo "scale=2; ( $mplongit * $deltalon ) / 1"|bc)
	xaxissize=$(echo "$landsizelat"|tr -d '.') ; yaxissize=$(echo "$landsizelon"|tr -d '.')
	xres=$(echo "$deltalat / $xaxissize"|bc -l) ; yres=$(echo "$deltalon / $yaxissize"|bc -l)
	printf "##########\n%b Resolution settings:%b latmax: %s latmin: %s latdelta: %s  - lonmax %s lonmin %s londelta %s \n%b Landsize:%b %s m / %s m ==> %s px / %s px \n%b Degrees/meter %b==> %s/%s\n##########\n" "$cya" "$def" "$maxlattit" "$minlattit" "$deltalat" "$maxlongit" "$minlongit" "$deltalon" "$cya" "$def" "$landsizelat" "$landsizelon" "$xaxissize" "$yaxissize" "$cya" "$def" "$xres" "$yres"
}
function f_drawtrees { # draw the trees
	query="select id from ${pproject}_trees"
	f_query
	echo '<g class="layer" id="Trees" inkscape:groupmode="layer"> <title>Trees</title>' >> "$pproject.svg"
	echo '<g class="layer" id="Trees Current" inkscape:groupmode="layer"> <title>Trees Current</title>' >> currenttrees.svg
	echo '<g class="layer" id="Trees Future" inkscape:groupmode="layer" style="display:none"> <title>Trees Future</title>' >> futuretrees.svg
	echo '<g class="layer" id="Trees Dead" inkscape:groupmode="layer" style="display:none"> <title>Trees Dead</title>' >> deadtrees.svg
	for j in "${sqlanswer[@]}"; do
		query="select id,speciesid,lattit,longit,currentwidth,currentheight,futurewidth,futureheight,alive from ${pproject}_trees where id=$j" && f_query2
		plantid="${results[0]}"; speciesid="${results[1]}"; plantlattit="${results[2]}";plantlongit="${results[3]}"
		svglattit=$(echo "($maxlattit - $plantlattit ) / $xres"|bc)
		svglongit=$(echo "($plantlongit - $minlongit) / $yres"|bc)
		currentwidth="${results[4]}"; currentheight="${results[5]}"; futurewidth="${results[6]}"; futureheight="${results[7]}"; alive="${results[8]}"
		query="select color from taxon_Plantae where id=$speciesid" && f_query2
		plantcolcod="${results}"
		if [[ "$flowercoloring" = "1" ]]; then
			query="select flowerperiod from ${pproject}_trees where id=$j;" && f_query
			for mmonth in ${sqlanswer[@]}; do
				if [[ "$mmonth" = "$curmonth" ]]; then
					query="select flowercolor from ${pproject}_trees where id=$j;" && f_query2
					if [[ "${results}" != "" ]] && [[ "${results}" != "NULL" ]]; then
						plantcolcod="${results}"
					fi
				fi
			done
		fi

		fontsize=$((currentwidth/4))
      cat <<EOT >> debug.svg
    <g id="d$plantid">
      <text y="$svglattit" x="$svglongit" opacity="0.6" text-anchor="middle" fill="#FF0000" font-size="$fontsize" font-family="Arial" dy=".3em">$j</text>
    </g>
EOT
    if [[ "$alive" = "1" ]]; then
  cat <<EOT >> currenttrees.svg
    <g id="$plantid">
      <circle cy="$svglattit" cx="$svglongit" opacity="0.3" fill="#$plantcolcod" r="$currentwidth" stroke="#000000" stroke-width="1"/>
      <text y="$svglattit" x="$svglongit" opacity="0.6" text-anchor="middle" fill="black" font-size="$fontsize" font-family="Arial" dy=".3em">$speciesid</text>
    </g>
EOT
    fontsize=$((futurewidth/4))
  cat <<EOT >> futuretrees.svg
    <g id="$plantid">
      <circle cy="$svglattit" cx="$svglongit" opacity="0.3" fill="#$plantcolcod" r="$futurewidth" stroke="#000000" stroke-width="1"/>
      <text y="$svglattit" x="$svglongit" opacity="0.6" text-anchor="middle" fill="black" font-size="$fontsize" font-family="Arial" dy=".3em">$speciesid</text>
    </g>
EOT
    else
      cat <<EOT >> deadtrees.svg
       <g id="$plantid">
        <circle cy="$svglattit" cx="$svglongit" opacity="0.3" fill="#000000" r="$currentwidth" stroke="#000000" stroke-width="1"/>
        <text y="$svglattit" x="$svglongit" opacity="0.6" text-anchor="middle" fill="black" font-size="$fontsize" font-family="Arial" dy=".3em">$speciesid</text>
       </g>
EOT
    fi
done
	echo '</g>' | tee -a currenttrees.svg |tee -a futuretrees.svg | tee -a debug.svg >> deadtrees.svg #end of layer
	cat currenttrees.svg >> "$pproject.svg" && rm currenttrees.svg
	cat futuretrees.svg >> "$pproject.svg" && rm futuretrees.svg
	cat deadtrees.svg >> "$pproject.svg" && rm deadtrees.svg
	echo '</g>' >> "$pproject.svg"
	cat debug.svg >> "$pproject.svg" && rm debug.svg
}
function f_drawpoly { # Anything that is not one plant (culture zone, house, path, electric wires, ponds, ...)
	echo "<g class=\"layer\" id=\"Paths\" inkscape:groupmode=\"layer\"> <title>Paths</title>" > paths.svg
	echo '<g class="layer" id="debug" inkscape:groupmode="layer" style="display:none"> <title>debug</title>' > debug.svg
	query="select distinct type from ${pproject}_poly"
	f_query
	for i in "${sqlanswer[@]}"; do
		echo "<g class=\"layer\" id=\"$i\" inkscape:groupmode=\"layer\"> <title>Poly $i</title>" >> "$pproject.svg"
		query="select polyid from ${pproject}_poly where type='$i';"
		f_query
		for j in "${sqlanswer[@]}"; do
			query="select polyid, type, name, color from ${pproject}_poly where polyid=$j"
			f_query2
			polyid="${results[0]}"
			ptype="${results[1]}"
			pname="${results[2]}"
			pcolor="${results[3]}"
			platts=();plongs=()
			posvec=()
			letter='<path d="M'
			query="select coordinates from ${pproject}_poly where polyid=$polyid"
			f_query2
			for k in "${results[@]}"; do
				plat=$(echo "$k"|cut -f1 -d',')
				plon=$(echo "$k"|cut -f2 -d',')
				platts+=("$plat"); plongs+=("$plon")
				svglat=$(echo "($maxlattit - $plat) / $xres"|bc)
				svglon=$(echo "($plon - $minlongit ) / $yres"|bc)
				if [[ "$ptype" = "paths" ]]; then
					posvec+=("$letter $svglon $svglat")
					letter=' L'
				else
					posvec+=("$svglon,$svglat")
				fi
			done
			if [[ "$ptype" = "paths" ]]; then
				echo "${posvec[*]}\" fill=\"none\" stroke=\"$pcolor\" stroke-width=\"10\"/>" >> paths.svg
			else
				n=${#platts[@]}
				sum=$( IFS="+"; bc <<< "${platts[*]}" ); txtlattit=$(echo "$sum/$n"|bc -l)
				sum=$( IFS="+"; bc <<< "${plongs[*]}" ); txtlongit=$(echo "$sum/$n"|bc -l)
				txtlongit=$(echo "${plongs[@]}" | awk '{ total += $2; count++ } END {printf "%.15g\n", total/count }')
				txtsvglat=$(echo "($maxlattit - $txtlattit ) / $xres"|bc)
				txtsvglon=$(echo "($txtlongit - $minlongit ) / $yres"|bc)
				ddata=("$polyid")
				if [[ "$ptype" != "culture" ]]; then
					if [[ "$ptype" = "parcel" ]]; then
					ddata=("")
				else
					ddata+=("$pname")
				fi
			fi
		cat <<EOT >>"$pproject.svg"
                <g id="p_$polyid">
                <polygon points="${posvec[@]}" opacity="0.4" fill="#$pcolor" stroke="#000000" stroke-width="1" />
                <text x="$txtsvglon" y="$txtsvglat" opacity="0.6" text-anchor="middle" fill="black" font-size="50px" font-family="Arial" dy=".3em">${ddata[@]}</text>
              </g>
EOT
        fi
	cat <<EOT >>debug.svg
        <g id="dp_$polyid">
          <text x="$txtsvglon" y="$txtsvglat" opacity="0.6" text-anchor="middle" fill="#FF0000" font-size="60px" font-family="Arial" dy=".3em">$j</text>
        </g>
EOT
		done
		if [[ "$ptype" = "paths" ]]; then echo '</g>' >> paths.svg;
		else echo '</g>' >> "$pproject.svg";fi
	done
	echo '</g>' >> paths.svg && cat paths.svg >> "$pproject.svg" && rm paths.svg
}
function f_drawlegend { # draw the legend
cat <<EOT >>"$pproject.svg"
    <g id="p_legend">
        <line x1="1050" y1="10" x2="1150" y2="10" stroke="red" stroke-width="1.9" />
        <text x="1065" y="45" opacity="0.6" text-anchor="left" fill="red" font-size="50px" font-family="Arial" dy=".3em">1m</text>
        <g id="compass">
          <path stroke="#000000" stroke-width="1.9" d="m1100 80 L 1150 180 L 1100 130 L 1050 180 Z" fill="#ffffff" />
          <path d="m1100 82 L 1149 179 L 1100 129 Z" fill="#FF0000" />
  </g>
EOT
query="select distinct speciesid from ${pproject}_trees;"
f_query
y="90"
for i in "${sqlanswer[@]}"; do
	query="select nomen$maplang from taxon_Plantae where id=$i" && f_query2 && nomenloc="${results[*]}"
	if [ -z "$nomenloc" ] || [ "$nomenloc" = "NULL" ] ; then #if we don't have the name in that language, print latin name
		query="select nomen from taxon_Plantae where id=$i" && f_query2 && nomenloc="${results[*]}"
	fi
	echo "<text x=\"25\" y=\"$y\" opacity=\"0.6\" text-anchor=\"left\" fill=\"black\" font-size=\"50px\" font-family=\"Arial\" dy=\".3em\">$i: $nomenloc</text>">>"$pproject.svg"
	y=$((y+45))
done
echo "</g>">>"$pproject.svg"
}
function f_genfile { # generates the file
txtid=28 && f_gettext && echo "$thistxt"
f_size_scale
for ffile in "$pproject.svg" "$pproject.svgz" currenttrees.svg futuretrees.svg deadtrees.svg paths.svg debug.svg; do # clean before we start
	if [[ -f "$ffile" ]]; then rm "$ffile"; fi
done
cat <<EOT >> "$pproject.svg"
<?xml version="1.0"?>
<svg width="$yaxissize" height="$xaxissize" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
EOT
f_drawpoly
f_drawtrees
f_drawlegend
echo "</svg>" >> "$pproject.svg"
gzip -k -S z "$pproject.svg"
}
#End of core

# projects management functions
function f_get_projectlist { # populates ${projects[@]}
	query="SHOW TABLES WHERE Tables_in_SimpleVeggieGarden LIKE '%_poly';"
	f_query
	pprojects=()
	for i in "${sqlanswer[@]}"; do pprojects+=("${i/_poly/}"); done
}
function f_list_projects { # prints $projects
	f_get_projectlist
	for i in "${pprojects[@]}"; do printf "%b%s%b\n" "$red" "$i" "$def"; done
}
function f_create_project { # inits tables for new project
	query="create table ${pproject}_trees (id INT AUTO_INCREMENT PRIMARY KEY, speciesid int NOT NULL, subsp_var varchar(50), alive boolean not null default 1, comment varchar(100), lattit DECIMAL(11,8) NOT NULL,longit DECIMAL(11,8) NOT NULL, currentwidth INT(4) , currentheight INT(5), futurewidth INT(4) NOT NULL, futureheight INT(5) NOT NULL, plantdate date, pruneperiod varchar(50), harvestperiod varchar(50), flowerperiod varchar(40), flowercolor varchar(10), CONSTRAINT ${pproject}_species FOREIGN KEY (speciesid) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT) ENGINE = InnoDB;" && f_query
	query="create table ${pproject}_poly (polyid int AUTO_INCREMENT PRIMARY KEY, type varchar(15), name varchar(15), color VARCHAR(10), coordinates varchar(4000));" && f_query
	query="create table ${pproject}_cultures (id INT AUTO_INCREMENT PRIMARY KEY, polyid int not null, startdate date DEFAULT NOW() not null, enddate date, plant1 int not null, plant2 int, plant3 int, plant4 int, plant5 int, plant6 int, plant7 int, plant8 int, notes varchar(2000), CONSTRAINT ${pproject}_polyid FOREIGN KEY (polyid) REFERENCES ${pproject}_poly (polyid) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant1 FOREIGN KEY (plant1) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant2 FOREIGN KEY (plant2) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant3 FOREIGN KEY (plant3) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant4 FOREIGN KEY (plant4) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant5 FOREIGN KEY (plant5) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant6 FOREIGN KEY (plant6) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant7 FOREIGN KEY (plant7) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT, CONSTRAINT ${pproject}_plant8 FOREIGN KEY (plant8) REFERENCES taxon_Plantae (id) ON DELETE CASCADE ON UPDATE RESTRICT) ENGINE = InnoDB;" && f_query
	query="create table ${pproject}_relations (id INT AUTO_INCREMENT PRIMARY KEY,projtable varchar(50),idproj int,taxontable varchar(50),idtaxon int,startdate date,enddate date,comment varchar(1000));" && f_query
	query="create table ${pproject}_events (id INT AUTO_INCREMENT PRIMARY KEY,projtable varchar(50),projid int,eventdate date,event varchar(1000),kilos int unsigned);" && f_query
}
function f_project_exists { # returns 0 if project exists
	query="select count(*) from information_schema.tables where table_schema='SimpleVeggieGarden' and table_name='${pproject}_poly';"
	f_query2 && if [[ "${results[0]}" = "1" ]]; then return 0; else return 1; fi
}
function ui_select_project { # select a project
	f_list_projects
	txtid=11 && f_gettext && printf "%b%s?%b\n" "$gre" "$thistxt" "$def" && read -r pproject
	if ! f_project_exists; then pproject="" && ui_select_project # if selected project does not exist, reset variable
	else f_size_scale # else run f_size_scale
	fi
}
function ui_create_project { # ui to init new project
	txtid=12 && f_gettext && printf "%b%s?%b\n" "$red" "$thistxt" "$def" && read -r pproject
	if [[ "$pproject" = "" ]]; then txtid=13 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def"; fi
	if ! f_project_exists ; then
		txtid=14 && f_gettext && printf "%b%s %s?%b\n" "$red" "$thistxt" "$pproject" "$def"
		read -r answer
		if [[ "$answer" = "y" ]]; then f_create_project;fi
	fi
}
function f_generate_html { # prints $projects
	f_get_projectlist
	printf "<html>\n<body>\n<h1>Simple Veggie Garden</h1>\n<p>Simple Interface.</p>\n" > index.html
	echo "<table>">>index.html
		for i in "${pprojects[@]}"; do
			echo "<tr><td><a href=\'genmap.php?pproject=$i\'></td><td>lol</td></tr>" >> index.html;
		done
	echo "</table>">>index.html
	printf "</body>\n</html>\n" >> index.html
}
function ui_delete_project { # ui to delete a project
	ui_select_project
	txtid=4 && f_gettext && printf "%b%s %s%b\n" "$red" "$thistxt" "$pproject" "$def"
	read -r cconfirm
	if [[ "$pproject" = "$cconfirm" ]]; then
		query="drop table ${pproject}_trees" && f_query
		query="drop table ${pproject}_cultures" && f_query
		query="drop table ${pproject}_poly" && f_query
	fi
	pproject=""
}
function ui_edit_project { # ui to enter in a project
	ui_select_project
	m_project_edit
}
# Project Functions
function f_add_tree { # function to add a tree
	query="INSERT INTO ${pproject}_trees (speciesid, subsp_var, comment, lattit, longit, currentwidth, currentheight, futurewidth, futureheight, plantdate, pruneperiod, harvestperiod, flowerperiod, flowercolor) VALUES ($speciesid, '$subsp_var', '$comment', $lattit, $longit, $currentwidth, $currentheight, $futurewidth, $futureheight, $plantdate, '$pruneperiod', '$harvestperiod', '$flowerperiod', '$flowercolor');" && f_query
}
function f_del_tree { # function to delete a tree
	query="delete from ${pproject}_trees where id=$treeid"
	treeid=""
}
function m_move_tree { #used to move a tree
	me_move_tree=("Move tree" "ui_move_bydistance" "ui_move_bycoords")
	f_menu "${me_move_tree[@]}"
}
function ui_move_bydistance { #used to move a tree by distance
	txtid=55 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r treeid # treeid
	query="select id from ${pproject}_trees where id=$treeid"
	f_query
	if [[ "${sqlanswer[0]}" = "" ]]; then return 1; fi
	txtid=56 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r ddirection && ddirection=${ddirection^^} # direction
	if   [[ "$ddirection" = "N" ]]; then axdist="$xres" && vvar="lattit"
	elif [[ "$ddirection" = "S" ]]; then axdist="-$xres" && vvar="lattit"
	elif [[ "$ddirection" = "E" ]]; then axdist="$yres" && vvar="longit"
	elif [[ "$ddirection" = "O" ]]; then axdist="-$yres" && vvar="longit"
	else return 2; fi
	txtid=57 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r ddistance # distance (cm)
	if ! [[ "$ddistance" =~ ^[0-9]+$ ]]; then return 3; fi
	echo "moving tree $treeid to the $ddirection by $ddistance cm"
	txtid=15 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r cconfirm && cconfirm=${cconfirm^^} # confirm
	if [[ "$cconfirm" = "Y" ]]; then
		query="select $vvar from ${pproject}_trees where id=$treeid" && f_query
		oldval="${sqlanswer[0]}"
		newval=$(echo "$oldval + ( $ddistance * $axdist )"|bc -l)
		query="update ${pproject}_trees set $vvar=$newval where id=$treeid" && f_query
	else return 4
	fi
}
function ui_move_bycoords { #used to move a tree by coordinates
	txtid=55 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r treeid # treeid
	query="select id from ${pproject}_trees where id=$treeid" && f_query
	if [[ "${sqlanswer[0]}" = "" ]]; then return 1 ;fi
	txtid=58 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r newcoords # newcoords
	newlat=$(echo "$newcoords"|cut -f1 -d',') && newlon=$(echo "$newcoords"|cut -f2 -d',')
	echo "moving tree $treeid to $newlat - $newlon"
	txtid=15 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r cconfirm && cconfirm=${cconfirm^^} # confirm
	if [[ "$cconfirm" = "Y" ]]; then query="update ${pproject}_trees set lattit=$newlat,longit=$newlon where id=$treeid" && f_query
	else return 4
	fi
}
function f_list_trees { # lists the trees
	query="select ${pproject}_trees.id, taxon_Plantae.nomen, ${pproject}_trees.subsp_var, ${pproject}_trees.lattit, ${pproject}_trees.longit,alive,comment from ${pproject}_trees join taxon_Plantae on taxon_Plantae.id=${pproject}_trees.speciesid order by ${pproject}_trees.id ASC;"
	f_query3
}
function f_add_poly { # function to add a poly
	query="insert into ${pproject}_poly (type, name, color, coordinates) VALUES ( '$ptype', '$pname', '$pcolor', '$pcoordinates');"
	f_query2
}
function f_getslope { # gets the slope between two points
	slope=$(echo "($svglongit1 - $svglongit2) / ($svglattit1 - $svglattit2)"| bc -l) && echo "$slope"
	slope=$(echo "($svglongit2 - $svglongit1) / ($svglattit2 - $svglattit1)"| bc -l) && echo "$slope"
}
function f_add_poly2 { # function to add a poly between two trees # NOT WORKING YET
	txtid=59 && f_gettext && printf "%b%s 1 %b\n" "$red" "$thistxt" "$def" && read -r treeid1
	query="select lattit from ${pproject}_trees where id=$treeid1" && f_query && lat1="${sqlanswer[0]}"
	query="select longit from ${pproject}_trees where id=$treeid1" && f_query && lon1="${sqlanswer[0]}"
	svglattit1=$(echo "($maxlattit - $lat1 ) / $xres"|bc)
	svglongit1=$(echo "($lon1 - $minlongit) / $yres"|bc)
	txtid=59 && f_gettext && printf "%b%s 2 %b\n" "$red" "$thistxt" "$def" && read -r treeid2
	query="select lattit from ${pproject}_trees where id=$treeid2" && f_query && lat2="${sqlanswer[0]}"
	query="select longit from ${pproject}_trees where id=$treeid2" && f_query && lon2="${sqlanswer[0]}"
	svglattit2=$(echo "($maxlattit - $lat2 ) / $xres"|bc)
	svglongit2=$(echo "($lon2 - $minlongit) / $yres"|bc)
	echo "$svglattit1 - $svglongit1 - $svglattit2 - $svglongit2"
	f_getslope
	echo "$slope"
	exit
}
function f_del_poly { # function to del a poly
	query="delete from ${pproject}_poly where polyid='$delpoly';" && f_query
}
function f_list_poly { #lists the poly
	query="select polyid, name, type from ${pproject}_poly;" && f_query3
}
function ui_common_add_tree {
	txtid=16 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r speciesid
	txtid=17 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r subsp_var
	txtid=18 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r comment
	txtid=21 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r currentwidth
	txtid=22 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r currentheight
	txtid=23 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r futurewidth
	txtid=24 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r futureheight
	txtid=25 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r plantdate
	txtid=26 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r pruneperiod
	txtid=27 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r harvestperiod
	txtid=60 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r flowerperiod
	txtid=61 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r flowercolor
	query="select nomen${maplang} from taxon_Plantae where id='$speciesid'" && f_query2 && specname="${results[*]}"
	printf "%-70s | %-70s\\n" "$specname" "$subsp_var"
	printf "%-40s | %-40s | %-40s| %-40s\\n" "$currentwidth" "$currentheight" "$futurewidth" "$futureheight"
	printf "%-40s | %-40s| %-40s\\n" "$plantdate" "$pruneperiod" "$harvestperiod"
	printf "%-70s | %-70s\\n" "$flowerperiod" "$flowercolor"
	printf "%-120s\\n" "$comment"
	txtid=14 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt?" "$def" && read -r answer
}
function ui_add_tree { # ui to add a tree
	txtid=19 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r lattit
	txtid=20 && f_gettext && printf "%b%s%b\n" "$gre" "$thistxt" "$def" && read -r longit
	ui_common_add_tree
	printf "%-70s | %-70s\\n" "$lattit" "$longit"
	if [[ "$answer" = "y" ]]; then f_add_tree; fi
}
function f_add_tree_line { # adds a line of n evenly spaced trees between p1 and p2
	steplat=$(echo "($point1lat-$point2lat)/($numberofplants-1)"|bc -l)
	steplon=$(echo "($point1lon-$point2lon)/($numberofplants-1)"|bc -l)
	i=1 && lattit="$point1lat" && longit="$point1lon"
	while [[ "$i" -le "$numberofplants" ]]; do
		f_add_tree
		lattit=$(echo 'scale=7;'"$lattit-($steplat)"|bc -l)
		longit=$(echo 'scale=7;'"$longit-($steplon)"|bc -l)
		((i++))
	done
}
function ui_del_tree { # ui to del a tree
	f_list_trees
	txtid=29 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r treeid
	txtid=30 && f_gettext && printf "%b%s%b %s%b\n" "$red" "$thistxt" "$gre" "$treeid" "$def" && read -r answer
	if [[ "$answer" = "y" ]]; then f_del_tree; fi
}
function ui_add_tree_line { # ui to add a line of n evenly spaced trees between p1 and p2
	txtid=62 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r point1lat
	txtid=63 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r point1lon
	txtid=64 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r point2lat
	txtid=65 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r point2lon
	txtid=66 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r numberofplants
	ui_common_add_tree
	printf "%bn=%s%b p1=%s,%s - p2=%s,%s%b\n" "$cya" "$numberofplants" "$red" "$point1lat" "$point1lon" "$point2lat" "$point2lon" "$def"
	if [[ "$answer" = "y" ]]; then f_add_tree_line; fi
}
function ui_add_poly { # ui to add a poly
	txtid=31 && f_gettext && echo "$thistxt"
	txtid=32 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r ptype
	txtid=33 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r pname
	txtid=34 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r pcolor
	txtid=35 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r pcoordinates
	txtid=36 && f_gettext && echo "$thistxt"
	txtid=37 && f_gettext && echo "$thistxt: $pname"
	txtid=38 && f_gettext && echo "$thistxt: $ptype"
	txtid=39 && f_gettext && echo "$thistxt: $pcolor"
	txtid=40 && f_gettext && echo "$thistxt: $pcoordinates"
	txtid=15 && f_gettext && printf "%b%s%b\n" "$mag" "$thistxt" "$def" && read -r answer
	if [[ "$answer" = "y" ]]; then f_add_poly;fi
}
function ui_del_poly { # ui to del a poly
	f_list_poly
	txtid=41 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r delpoly
	txtid=42 && f_gettext && printf "%b%s%b %s%b\n" "$red" "$thistxt" "$gre" "$delpoly" "$def" && read -r answer
	if [[ "$answer" = "y" ]]; then f_del_poly;fi
}
# Menus + species functions
function ui_add_species { # ui to add a species
	txtid=5 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r namelat
	txtid=6 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r nameen
	txtid=7 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r namefr
	txtid=8 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r namees
	txtid=9 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r color
	printf "%b namelat = %s\n nameen = %s\n namefr = %s\n namees = %s\n color = %s%b\n" "$gre" "$namelat" "$nameen" "$namefr" "$namees" "$color" "$def"
	txtid=15 && f_gettext && printf "%b%s%b\n" "$red" "$thistxt" "$def" && read -r cconfirm
	if [[ "$cconfirm" = "y" ]]; then
		query="INSERT INTO taxon_Plantae (nomen, nomen_en, nomen_fr, nomen_es, color) VALUES ( '$namelat', '$nameen', '$namefr', '$namees', '$color');"
		f_query
	fi
}
function f_list_species { # lists the species
	query="select id, nomen, nomen${maplang} from taxon_Plantae" && f_query3
}
function m_trees { # manage trees
	me_trees=("Edit $pproject trees" "f_list_trees" "ui_add_tree" "ui_add_tree_line" "ui_del_tree" "m_move_tree" "f_list_species")
	f_menu "${me_trees[@]}"
}
function m_poly { # manage poly
	me_poly=("Edit $pproject poly" "f_list_poly" "ui_add_poly" "ui_del_poly")
	f_menu "${me_poly[@]}"
}
function m_project_edit { # menu to deit a project
	me_project_edit=("Edit $pproject" "m_trees" "m_poly" "f_genfile")
	f_menu "${me_project_edit[@]}"
}
function m_projects { # projects menu
	me_projects=("Projects Menu" "f_list_projects" "ui_create_project" "ui_delete_project" "ui_edit_project")
	f_menu "${me_projects[@]}"
}
function m_species { # species menu
	me_species=("Species Menu" "ui_add_species" "f_list_species")
	f_menu "${me_species[@]}"
}
function m_main { # Main Menu (displayed if called without args)
	me_main=("SimpleVeggieGarden" "m_projects" "m_species" "f_backup_db" "f_generate_html")
	f_menu "${me_main[@]}"
}
# script entry point
if [[ "$pproject" = "" ]]; then # if no argument, launch main menu, else check if argument is a project name and compile latest archive of this project
	m_main && printf "%bbye%b\n" "$blu" "$def" && exit
#elif [[ "$pproject" = "test" ]]; then
#	pproject="Terrassa" && f_size_scale
#	f_add_poly2 && exit
else
	if f_project_exists ; then
		f_genfile
	fi
fi
