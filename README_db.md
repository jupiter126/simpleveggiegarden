# About the database:
When generating the taxon database, I was confronted with two hard choices:
1. Data to generate
 - Generate a partial DB, with only the taxons which had french names: perfect for human beings, but useless for scientists.
 - Generate a complete DB, with all taxons: perfect for scientists, too much for human beings.

While I include a DB with this script (the one I am using), taxondb.sh is what I used to generate it: feel free to build your own custom taxon db.
Be aware that the projects are based on taxondb, so if you export a project, do it with the taxondb you reference to in the project!

2. Avoiding double entries:
   Some references are entered various times with different names, for example:
  MariaDB SimpleVeggieGarden> select * from taxon_Plantae where nomen like 'olea eu%';
  +------+--------------------------------------+----------+-----------------+----------+--------+--------+---------------+----------+----------+-------------+--------+----------------+--------------+------+---------+----------+----------+----------+
  | id   | nomen                                | nomen_en | nomen_fr        | nomen_es | color  | phylum | classe        | ordre    | famille  | sousfamille | tribu  | group1inpm     | group2inpm   | rang | habitat | statutfr | favorite | source   |
  +------+--------------------------------------+----------+-----------------+----------+--------+--------+---------------+----------+----------+-------------+--------+----------------+--------------+------+---------+----------+----------+----------+
  | 7154 | Olea europaea L., 1753               | NULL     | Olivier dEurope | NULL     | 3fd753 |        | Equisetopsida | Lamiales | Oleaceae |             | Oleeae | Trachéophytes  | Angiospermes | ES   |       3 | P        |     NULL | taxreffr |
  | 7155 | Olea europaea var. europaea L., 1753 | NULL     | Olivier         | NULL     | aff2b8 |        | Equisetopsida | Lamiales | Oleaceae |             | Oleeae | Trachéophytes  | Angiospermes | VAR  |       3 | M        |     NULL | taxreffr |
  +------+--------------------------------------+----------+-----------------+----------+--------+--------+---------------+----------+----------+-------------+--------+----------------+--------------+------+---------+----------+----------+----------+
  By merging different databases, I end up with doubles anyway... so up to the user to be consistent with the one he uses!

  J'ai décidé de laisser 3 options aux utilisateurs:
   1: Utiliser une base de données édulcorée précompilée (recommandé) - 24000 taxons de tous regnes
     - Les doublons sur les nom (fr/en/es)  sont interdits (aka: il n'y a qu'un 'saxifrage') 
     - Les entrées qui n'on pas au moins un nom non latin ne sont pas gardées.
   2: Utiliser une base de données "scientifique" précompilée (277000 taxons de tous regnes)
     - Cette table reprends moins de données que le fichier dont il a été extrait, mais avec le nom latin et la source - cela devrait suffire.
     - Je n'ai gardé que les entrées dont le nom scientifique est accepté (pas les appélations anciennes)
   3: Générer sa propre base de données, ce pour quoi j'ai partagé taxondb.sh à titre d'exemple (projets spécifiques).

  Conséquences: 
  Deux projets utilisant deux bases de taxons différents ne seront pas "compatibles":
   - Quand on est "monsieur tout le monde" (mode humain), on utilise la base de taxons édulcorée précompilée - et on a pas besoins de se poser de questions!
   - Quand on partage des mesures scientifiques (mode scientifique), on partage sa db de taxons avec le projet!



Description de la base de données:
nomdelatable: description
Objectif futur (optionnel)
description technique.

UI: contient les textes dans différentes langues
+--------+--------------+------+-----+---------+----------------+
| Field  | Type         | Null | Key | Default | Extra          |
+--------+--------------+------+-----+---------+----------------+
| id     | int(11)      | NO   | PRI | NULL    | auto_increment |
| txt_en | varchar(500) | NO   |     | NULL    |                |
| txt_fr | varchar(500) | YES  |     | NULL    |                |
| txt_es | varchar(500) | YES  |     | NULL    |                |
+--------+--------------+------+-----+---------+----------------+

specs_habitats: contient la liste des habitats de taxref, en différentes langues. J'ai ajoute l'habitat 9: "NULL" pour quand on ne sait pas (import d'une autre db)
+---------+--------------+------+-----+---------+----------------+
| Field   | Type         | Null | Key | Default | Extra          |
+---------+--------------+------+-----+---------+----------------+
| id      | int(11)      | NO   | PRI | NULL    | auto_increment |
| nom_fr  | varchar(50)  | YES  |     | NULL    |                |
| desc_fr | varchar(400) | YES  |     | NULL    |                |
| nom_en  | varchar(50)  | YES  |     | NULL    |                |
| desc_en | varchar(400) | YES  |     | NULL    |                |
| nom_es  | varchar(50)  | YES  |     | NULL    |                |
| desc_es | varchar(400) | YES  |     | NULL    |                |
+---------+--------------+------+-----+---------+----------------+

specs_rangs= contient les rangs et codes associés, en différentes langues. J'ai ajouté le rang 1000: Inconnu, pour quand on ne sait pas (import d'une autre db)
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| refid    | int(11)     | YES  | UNI | NULL    |                |
| code     | varchar(10) | YES  | UNI | NULL    |                |
| nomen_fr | varchar(50) | YES  |     | NULL    |                |
| nomen_en | varchar(50) | YES  |     | NULL    |                |
| nomen_es | varchar(50) | YES  |     | NULL    |                |
+----------+-------------+------+-----+---------+----------------+

specs_relations: donne des relations positives/négatives entre familles/genres/classes (ex: les apiacées aiment bien être pres des aliacées, mais pas des légumineuses). (Besoin de rappeler que je ne suis pas botaniste: cette table est vide pour l'instant)
Objectif très long terme: que le programme puisse faire des recommandations (ajouter ou rajouter une plante dans une zone par exemple)
Cette table permet de lier des groupes différents selon les besoins: 
- la carotte a un effet positif sur la chèvre, mais la chèvre un effet négatif sur la carotte.
- le mildiou a un effet négatif sur la tomate, mais la tomate a un effet positif sut le mildiou ( ==> aide en prévention de la transmission de maladies)
+----------------+---------------------+------+-----+---------+----------------+
| Field          | Type                | Null | Key | Default | Extra          |
+----------------+---------------------+------+-----+---------+----------------+
| id             | int(11)             | NO   | PRI | NULL    | auto_increment |
| taxon1table    | varchar(50)         | YES  |     | NULL    |                |
| taxon1column   | varchar(50)         | YES  |     | NULL    |                |
| taxon1nomen    | varchar(200)        | YES  |     | NULL    |                |
| taxon1affinity | tinyint(3) unsigned | YES  |     | NULL    |                |
| taxon2table    | varchar(50)         | YES  |     | NULL    |                |
| taxon2column   | varchar(50)         | YES  |     | NULL    |                |
| taxon2nomen    | varchar(200)        | YES  |     | NULL    |                |
| taxon2affinity | tinyint(3) unsigned | YES  |     | NULL    |                |
+----------------+---------------------+------+-----+---------+----------------+

taxon_*: Les tables de taxons de différents règnes.
J'ai décidé de les séparer, car cela permet de travailler sur des jeux de données spécifiques (plus léger ou plus complets selon le champ d'application du projet).
Ces tables sont toutes structurées exactement de la même manière: (taxon_Animalia, taxon_Archaea, taxon_Bacteria, taxon_Chromista, taxon_Fungi, taxon_Plantae, taxon_Protozoa)
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| nomen       | varchar(400) | NO   | UNI | NULL    |                |
| nomen_en    | varchar(250) | YES  | UNI | NULL    |                |
| nomen_fr    | varchar(250) | YES  | UNI | NULL    |                |
| nomen_es    | varchar(250) | YES  | UNI | NULL    |                |
| color       | varchar(10)  | YES  |     | NULL    |                |
| phylum      | varchar(50)  | YES  |     | NULL    |                |
| classe      | varchar(50)  | YES  |     | NULL    |                |
| ordre       | varchar(50)  | YES  |     | NULL    |                |
| famille     | varchar(50)  | YES  |     | NULL    |                |
| sousfamille | varchar(50)  | YES  |     | NULL    |                |
| tribu       | varchar(50)  | YES  |     | NULL    |                |
| group1inpm  | varchar(50)  | YES  |     | NULL    |                |
| group2inpm  | varchar(150) | YES  |     | NULL    |                |
| rang        | varchar(10)  | YES  | MUL | NULL    |                |
| habitat     | int(11)      | YES  | MUL | NULL    |                |
| statutfr    | varchar(10)  | YES  |     | NULL    |                |
| favorite    | tinyint(1)   | YES  |     | NULL    |                | --> permet à l'utilisateur d'indiquer ses favorits (facilité de sélection pour l'encodage)
| source      | varchar(50)  | YES  |     | NULL    |                | --> source indique ou on a été chercher les données pour populer la base de données.
+-------------+--------------+------+-----+---------+----------------+

tempmerge: la table temporaire utilisée pour fusionner des données à la base de données principale.

Viennent ensuite les tables spécifiques à chaque projet:
Projet_trees: répertorie les plantes pérennes d'un projet avec leur position gps.
+---------------+----------------+------+-----+---------+----------------+
| Field         | Type           | Null | Key | Default | Extra          |
+---------------+----------------+------+-----+---------+----------------+
| id            | int(11)        | NO   | PRI | NULL    | auto_increment | - id unique de cette plante spécifique
| speciesid     | int(11)        | NO   | MUL | NULL    |                | - id de référence de dans la table taxon_Plantae (10844 pour un pommier par exemple --> Malus Domestica 
| subsp_var     | varchar(50)    | YES  |     | NULL    |                | - Red Cox, Pink Lady, ... permet de noter un cultivar spécifique
| alive         | tinyint(1)     | NO   |     | 1       |                | - 1 si l'arbre est vivant (permet par exemple de répertorier les souches)
| comment       | varchar(100)   | YES  |     | NULL    |                | - commentaire sur la plante (exemples: "cadeau de Tatie Danielle", "mort de secheresse", "ne pousse pas", "fruits infects", ...)
| lattit        | decimal(26,22) | NO   |     | NULL    |                | - lattitude de la plante
| longit        | decimal(26,22) | NO   |     | NULL    |                | - longitude de la plante
| currentwidth  | int(4)         | YES  |     | NULL    |                | - rayon actuel (optionnel, pratique pour dessiner)
| currentheight | int(5)         | YES  |     | NULL    |                | - hauteur actuelle (optionnel, actuellement 100% inutile ... un jour j'espere pouvoir gérer le multiétagé)
| futurewidth   | int(4)         | NO   |     | NULL    |                | - rayon futur (optionnel, pratique pour prévoir à qui cela ressemblera plus tard)
| futureheight  | int(5)         | NO   |     | NULL    |                | - hauteur future (optionnel, actuellement 100% inutile ... un jour j'espere pouvoir gérer le multiétagé)
| plantdate     | date           | YES  |     | NULL    |                | - date de plantation, format 20211225 si on plante a noël
| pruneperiod   | varchar(50)    | YES  |     | NULL    |                | - époque de taille (pour la planification du calendrier)
| harvestperiod | varchar(50)    | YES  |     | NULL    |                | - époque de récolte  (pour la planification du calendrier)
| flowerperiod  | varchar(40)    | YES  |     | NULL    |                | - époque de floraison
| flowercolor   | varchar(10)    | YES  |     | NULL    |                | - couleur des fleurs
+---------------+----------------+------+-----+---------+----------------+

Projet_poly: répertorie les "poly" d'un projet
+-------------+---------------+------+-----+---------+----------------+
| Field       | Type          | Null | Key | Default | Extra          |
+-------------+---------------+------+-----+---------+----------------+
| polyid      | int(11)       | NO   | PRI | NULL    | auto_increment | --> id unique du poly
| type        | varchar(15)   | YES  |     | NULL    |                | --> type de poly. Les types sont: parcel (pour une parcelle), water (pour de l'eau), paths (pour les chemins), culture (pour les zones de culture), misc (pour les autres).  Chez moi, je déclare un poly par "butte", mais on pourrait définir un poly par champs pour la "grande culture".  Le type "paths", n'est autre qu'une suite de points reliés par des lignes ce n'est pas un polygone car pas fermé.
| name        | varchar(15)   | YES  |     | NULL    |                | --> nom a afficher sur le poly
| color       | varchar(10)   | YES  |     | NULL    |                | --> couleur du poly
| coordinates | varchar(4000) | YES  |     | NULL    |                | --> coordonnées, sous forme: '32.6420558187774,4.2117970443408573 32.64208450628714,4.2118361946987773 32.642056451824126,4.2118902797034584 32.64201818658267,4.2118564897801887'
+-------------+---------------+------+-----+---------+----------------+

Projet_cultures: répertorie les cultures d'un projet (liée à un poly)
Une culture se déroule sur un polygone de type "culture", elle a une date de début et une date de fin.
On y mets différentes plantes (lien souhaité avec specs_relations), puis après on peut y prendre des notes.
Après quelques saisons, on obtient une vision historique de ce qui se passe sur cette zone (ah, j'y ai mis des oignons il y a moins de 3 ans... attendre avant d'en remettre).
+-----------+---------------+------+-----+---------------------+----------------+
| Field     | Type          | Null | Key | Default             | Extra          |
+-----------+---------------+------+-----+---------------------+----------------+
| id        | int(11)       | NO   | PRI | NULL                | auto_increment | --> id unique de la culture
| polyid    | int(11)       | NO   | MUL | NULL                |                | --> id du poly ou elle se déroule
| startdate | date          | NO   |     | current_timestamp() |                | --> date de début
| enddate   | date          | YES  |     | NULL                |                | --> date de fin
| plant1    | int(11)       | NO   | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant2    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant3    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant4    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant5    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant6    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant7    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| plant8    | int(11)       | YES  | MUL | NULL                |                | --> lien vers taxon_Plantae
| notes     | varchar(2000) | YES  |     | NULL                |                | --> Parce que oui, on note ce qui s'est passé sur une zone: quantités récoltées, problèmes de secheresse, faim d'azote, compaction, ...)
+-----------+---------------+------+-----+---------------------+----------------+

Projet_events: répertorie les évènements sur tree ou poly
+-----------+------------------+------+-----+---------+----------------+
| Field     | Type             | Null | Key | Default | Extra          |
+-----------+------------------+------+-----+---------+----------------+
| id        | int(11)          | NO   | PRI | NULL    | auto_increment | --> id unique de l'évènement
| projtable | varchar(50)      | YES  |     | NULL    |                | --> sur quelle table s'est déroulé l'évènement
| projid    | int(11)          | YES  |     | NULL    |                | --> id de la plante ou du poly
| eventdate | date             | YES  |     | NULL    |                | --> date de l'évènement
| event     | varchar(1000)    | YES  |     | NULL    |                | --> description (arbre: taille, récolte, trogne, ététage, plessage, paillage, .... poly: paillage, labour, sous-solage, jachère, pature, ...
| kilos     | int(10) unsigned | YES  |     | NULL    |                | --> pour les arbres ou grandes cultures, ou cela peut avoir du sens de comparer différentes années.  Pour les butes de permaculture, on préfèrera noter cela dans les "notes" de culture (12kg de tomates sur la plante a coté de l'arbre, 25 sur celle au mileu de la bute)
+-----------+------------------+------+-----+---------+----------------+

Projet_relations: répertorie les maladies, champignons, ravageurs, ... constatés
A la différence de la table specs_relations, qui contient les affinités génériques entre différents taxons, cette table-ci représente les observations concrètes sur une plante.
Par exemple un orme atteint de graphiose, ou un pommier rongé de l'intérieur par des fourmis (ne me demandez pas pourquoi des exemples si concrets!).
Cela permet de suivre l'évolution de pathologies (les symbioses positives passent souvent plus inapercues, car on ne se pose pas de questions quand tout va bien)
+------------+---------------+------+-----+---------+----------------+
| Field      | Type          | Null | Key | Default | Extra          |
+------------+---------------+------+-----+---------+----------------+
| id         | int(11)       | NO   | PRI | NULL    | auto_increment | --> L'id unique de la relation
| projtable  | varchar(50)   | YES  |     | NULL    |                | --> la table du sujet concerné (tree (champignons, insectes, ...) ou poly (champ infesté par les rongeurs, ...)
| idproj     | int(11)       | YES  |     | NULL    |                | --> L'id unique de l'arbre ou du poly concerné
| taxontable | varchar(50)   | YES  |     | NULL    |                | --> la table de taxons du "coupable"
| idtaxon    | int(11)       | YES  |     | NULL    |                | --> l'id unique du taxon incriminé
| startdate  | date          | YES  |     | NULL    |                | --> date de la constatation
| enddate    | date          | YES  |     | NULL    |                | --> date de fin
| comment    | varchar(1000) | YES  |     | NULL    |                | --> commentaires (ex: "les renards on reglé le soucis de rongeurs", "les fourmis ont fini par déménager après avoir tué l'arbre", "tout passé au glyphosate")
+------------+---------------+------+-----+---------+----------------+

