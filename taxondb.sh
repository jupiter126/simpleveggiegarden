#!/usr/local/bin/bash
## #!/bin/bash
# todo --> Merge canadensys data: same lattitudes and languages (there should be collisions) - once this works then anything is possible
#mmode="scientific" # db includes all unique, valid taxons
mmode="human" # db only includes taxons that have at least one human (non-latin) name!
source commonconf.settings.sh
function f_warning {
	txtid=43 && f_gettext && echo "$red$thistxt$def"
	txtid=44 && f_gettext && echo "$red$thistxt$def"
	txtid=45 && f_gettext && read -r -p "$red$thistxt $gre yes $def" aanswer
	if [[ "$aanswer" != "yes" ]]; then exit; fi
}
function f_summary {
	query="show tables like 'taxon%';" && f_query
	totaltaxon="0"
	for ttaxon in "${sqlanswer[@]}"; do
		query="select count(id) from $ttaxon" && f_query && vartot="${sqlanswer[*]}"
		query="select count(id) from $ttaxon where nomen_fr!='' and nomen_en!='' and nomen_es!='';" && f_query && vardone="${sqlanswer[*]}"
		query="select count(id) from $ttaxon where nomen_fr!='' and nomen_en!='';" && f_query && varenfr="${sqlanswer[*]}"
		query="select count(id) from $ttaxon where nomen_fr!='';" && f_query && varfr="${sqlanswer[*]}"
		query="select count(id) from $ttaxon where nomen_en!='';" && f_query && varen="${sqlanswer[*]}"
		query="select count(id) from $ttaxon where nomen_es!='';" && f_query && vares="${sqlanswer[*]}"
		totaltaxon="$((totaltaxon+vartot))"
		echo "$cya $ttaxon has $vartot records, $vardone have en+fr+es, $varenfr have fr+en, $varfr have fr, $varen have en and $vares have es"
	done
	echo "$yel Total Taxons: $totaltaxon $def"
}
function f_randgreen { # from https://stackoverflow.com/questions/1586147/how-to-generate-random-greenish-colors - https://stackoverflow.com/questions/2556190/random-number-from-a-range-in-a-bash-script - and https://www.unix.com/shell-programming-and-scripting/268487-awk-unix-random-rgb-colors-generator.html
	greenval=$(shuf -i 100-256 -n 1); redval=$(shuf -i 20-$((greenval - 60)) -n 1); blueval=$(shuf -i $((redval-20))-$((redval+20)) -n 1)
	color=$(printf "%02x%02x%02x\\n" "$redval" "$greenval" "$blueval")
}
function f_createtaxontable { # creates table for a new reign
	txtid=46 && f_gettext && echo "$yel$thistxt$def $ttable"
	if [[ "$mmode" = "scientific" ]]; then
		query="CREATE TABLE $ttable ( id INT AUTO_INCREMENT PRIMARY KEY, nomen VARCHAR(400) unique NOT NULL, nomen_en VARCHAR(250), nomen_fr VARCHAR(250), nomen_es VARCHAR(250), color VARCHAR(10), phylum VARCHAR(50), classe VARCHAR(50), ordre VARCHAR(50), famille VARCHAR(50), sousfamille VARCHAR(50), tribu VARCHAR(50), group1inpm VARCHAR(50), group2inpm VARCHAR(150), rang VARCHAR(10), habitat int, statutfr VARCHAR(10), favorite boolean default NULL, source VARCHAR(50), constraint a_$regne FOREIGN KEY b_$regne (rang) REFERENCES specs_rangs (code), constraint c_$regne FOREIGN KEY d_$regne (habitat) REFERENCES specs_habitats (id)) ENGINE = InnoDB;" && f_query
	elif [[ "$mmode" = "human" ]];then
		query="CREATE TABLE $ttable ( id INT AUTO_INCREMENT PRIMARY KEY, nomen VARCHAR(400) unique NOT NULL, nomen_en VARCHAR(250) unique, nomen_fr VARCHAR(250) unique, nomen_es VARCHAR(250) unique, color VARCHAR(10), phylum VARCHAR(50), classe VARCHAR(50), ordre VARCHAR(50), famille VARCHAR(50), sousfamille VARCHAR(50), tribu VARCHAR(50), group1inpm VARCHAR(50), group2inpm VARCHAR(150), rang VARCHAR(10), habitat int, statutfr VARCHAR(10), favorite boolean default NULL, source VARCHAR(50), constraint a_$regne FOREIGN KEY b_$regne (rang) REFERENCES specs_rangs (code), constraint c_$regne FOREIGN KEY d_$regne (habitat) REFERENCES specs_habitats (id)) ENGINE = InnoDB;" && f_query
	else
		echo "mmode must be set to scientific or human!"
	fi
}
function f_record_exists { # returns 0 if record with "$nomen" exists in $ttable (1 if not)
	query="select nomen from $ttable where nomen='$nomen';"
	f_query
	if [[ "${sqlanswer[*]}" = "" ]]; then return 1; else return 0; fi
}
function f_record_exists_fr { # returns 0 if record with "$nomen_fr" exists in $ttable (1 if not)
	query="select nomen_fr from $ttable where nomen_fr='$nomen_fr';"
	f_query
	if [[ "${sqlanswer[*]}" = "" ]] || [[ $nomen_fr = "NULL" ]]; then return 1; else return 0; fi
}
function f_record_exists_en { # returns 0 if record with "$nomen_en" exists in $ttable (1 if not)
	query="select nomen_en from $ttable where nomen_en='$nomen_en';"
	f_query
	if [[ "${sqlanswer[*]}" = "" ]] || [[ $nomen_en = "" ]]; then return 1; else return 0; fi
}
function f_table_exists { # returns 0 if table "$ttable" exists (1 if not)
	query="SHOW TABLES LIKE '$ttable';"
	f_query
	if [[ "${sqlanswer[*]}" = "" ]]; then return 1; else declare "check_$ttable"=1 && return 0; fi
}
function f_create_taxreffr { # Procedure to import taxon from France. Source: https://inpn.mnhn.fr/programme/referentiel-taxonomique-taxref
	f_warning # comment to disable warning
	# 1. Get latest version of zip file from https://inpn.mnhn.fr/telechargement/referentielEspece/taxref/14.0/menu
	if [[ -f TAXREF_V14_2020.zip ]] ; then unzip TAXREF_V14_2020.zip; fi
	ssource=taxreffr
	nomen_es="NULL" # not available in this db
	ttable="specs_relations"
	if ! f_table_exists; then
		query="create table $ttable (id INT AUTO_INCREMENT PRIMARY KEY,taxon1table varchar(50),taxon1column varchar(50), taxon1nomen varchar(200),taxon1affinity tinyint unsigned check (0<=taxon1affinity<=10), taxon2table varchar(50),taxon2column varchar(50), taxon2nomen varchar(200),taxon2affinity tinyint unsigned check (0<=taxon2affinity<=10));" && f_query
	fi
	function f_create_taxref_habitats { # creer et remplir la table specs_habitats
		ttable="specs_habitats"
		if ! f_table_exists; then # Si la table n'existe pas
			if [[ -f habitats_note.csv ]]; then # Si le fichier existe (dans le répertoire ou on exécute ce sript)
				txtid=47 && f_gettext && echo "$gre$thistxt$def $ttable"
				query="create table $ttable (id INT AUTO_INCREMENT PRIMARY KEY unique not null, nom_fr VARCHAR(50), desc_fr VARCHAR(400), nom_en VARCHAR(50), desc_en VARCHAR(400), nom_es VARCHAR(50), desc_es VARCHAR(400)) ENGINE = InnoDB;" && f_query
				recode CP1252..utf8 < habitats_note.csv > habitats_note2.csv
				sed -e 's/^\|$/"/g' -e 's/\t/"\t"/g' -e "s/[']//g" < habitats_note2.csv > habitats_note.txt
				while IFS=';' read -r -a myArray; do
					nomfr=${myArray[1]//\"}
					descfr=${myArray[2]//\"}
					if [[ "$nomfr" != "LB_HABITAT" ]] ; then
						query="insert into $ttable (nom_fr,desc_fr) values ('$nomfr','$descfr');"
						f_query
					fi
				done < habitats_note.txt && rm habitats_note.txt habitats_note2.csv
				# translation part...done manually
				query="update $ttable set nom_en='Marine', desc_en='Species living only in a marine environment.', nom_es='Marino', desc_es='Especies viviendo solo en un entorno marino.' where id=1;" && f_query
				query="update $ttable set nom_en='Fresh Water', desc_en='Species living only in a fresh water environment', nom_es='Agua dulce', desc_es='Especies viviendo solo en entorno de agua dulce.' where id=2;" && f_query
				query="update $ttable set nom_en='Terrestrial', desc_en='Species living only in a terrestrial environment', nom_es='Terrestro', desc_es='Especies viviendo solo en entorno terrestro' where id=3;" && f_query
				query="update $ttable set nom_en='Marine and fresh water', desc_en='Species living part of their cycle in fresh water and another part in the sea (diadroms, amphidroms, anadroms ou catadroms)', nom_es='Marino y agua dulce', desc_es='Especies viviendo parte de su cyclo en agua dulce y otra part en el mar (diadromas, amphidromas, anadromas o catadromas)' where id=4;" && f_query
				query="update $ttable set nom_en='Marine and terrestrial', desc_en='Species living part of their cycle in the sea and another part on land (pinnipedes, tortoise or seabirds for example)', nom_es='Marino y terrestro', desc_es='Especies viviendo parte de su cyclo en el mar y otra part en entorno terrestro (pinnipedas, tortugas o aves marinos por ejemplo)' where id=5;" && f_query
				query="update $ttable set nom_en='Brackish water', desc_en='Species living only in brackish waters', nom_es='Agua salobre', desc_es='Especies viviendo solo en agua salobre' where id=6;" && f_query
				query="update $ttable set nom_en='Continental (terrestrial and/or fresh water)', desc_en='Continental (non-marine) species of which we do not know if they are terrestrial and/or fresh water (taxons from Fauna Europea).', nom_es='Continental (terrestro y/o de agua dulce)', desc_es='Especies continentales (no marinas) para cuales no sabemos si son terrestras y/o de agua dulce (taxones con fuente Fauna Europea).' where id=7;" && f_query
				query="update $ttable set nom_en='Continental (terrestrial and fresh water)', desc_en='Land species having part of their lifecycle in fresh water (odonates for example), or strongly linked to the aquatic environment (otter for example).', nom_es='Continental (terrestro y de agua dulce)', desc_es='Especies terrestras que tienen parte de su cyclo en agua dulce (odonatas por ejemplo) o que tienen un enlace fuerte con el entorno aquatico (nutria por ejemplo).' where id=8;" && f_query
				query="insert into $ttable (nom_fr,desc_fr,nom_en,desc_en,nom_es,desc_es) values ('NULL','NULL','NULL','NULL','NULL','NULL');" && f_query
			else
				txtid=48 && f_gettext && echo "$red$thistxt$def"
			fi
		fi
	}
	function f_create_taxref_rangs { # creer et remplir la table specs_rangs
		ttable="specs_rangs"
		if ! f_table_exists; then # Si la table n'existe pas
			if [[ -f rangs_note.csv ]]; then # Si le fichier existe (dans le répertoire ou on exécute ce sript)
				txtid=47 && f_gettext && echo "$gre$thistxt$def $ttable"
				query="create table $ttable (id INT AUTO_INCREMENT PRIMARY KEY, refid int unique, code varchar(10) unique, nomen_fr varchar(50), nomen_en varchar(50), nomen_es varchar(50)) ENGINE = InnoDB;" && f_query
				recode CP1252..utf8 < rangs_note.csv > rangs_note2.csv
				sed -e 's/^\|$/"/g' -e 's/\t/"\t"/g' -e "s/[']//g" < rangs_note2.csv > rangs_note.txt
				while IFS=';' read -r -a myArray; do
					refid=${myArray[0]//\"}
					code=${myArray[1]//\"}
					nom_fr=${myArray[2]//\"}
					nom_en=${myArray[3]//\"}
					if [[ "$refid" != "RG_LEVEL" ]] ; then
						query="insert into $ttable (refid,code,nomen_fr,nomen_en) values ('$refid','$code','$nom_fr','$nom_en');" && f_query
					fi
				done < rangs_note.txt && rm rangs_note.txt rangs_note2.csv
				# Spanish translation is approximative at best!
				query="update specs_rangs set nomen_es='Dominio' where id=1;" && f_query
				query="update specs_rangs set nomen_es='Super-reinado' where id=2" && f_query
				query="update specs_rangs set nomen_es='Reinado' where id=3" && f_query
				query="update specs_rangs set nomen_es='Sub-reinado' where id=4" && f_query
				query="update specs_rangs set nomen_es='Infra-reinado' where id=5" && f_query
				query="update specs_rangs set nomen_es='Phylum' where id=6" && f_query
				query="update specs_rangs set nomen_es='Sub-phylum' where id=7" && f_query
				query="update specs_rangs set nomen_es='Infra-phylum' where id=8" && f_query
				query="update specs_rangs set nomen_es='Division' where id=9" && f_query
				query="update specs_rangs set nomen_es='Sub-division' where id=10" && f_query
				query="update specs_rangs set nomen_es='Super-class' where id=11" && f_query
				query="update specs_rangs set nomen_es='Clade' where id=12" && f_query
				query="update specs_rangs set nomen_es='Clase' where id=13" && f_query
				query="update specs_rangs set nomen_es='Sub-clase' where id=14" && f_query
				query="update specs_rangs set nomen_es='Infra-clase' where id=15" && f_query
				query="update specs_rangs set nomen_es='Parv-clase' where id=16" && f_query
				query="update specs_rangs set nomen_es='Legio' where id=17" && f_query
				query="update specs_rangs set nomen_es='Super-orden' where id=18" && f_query
				query="update specs_rangs set nomen_es='Cohorta' where id=19" && f_query
				query="update specs_rangs set nomen_es='Orden' where id=20" && f_query
				query="update specs_rangs set nomen_es='Sub-orden' where id=21" && f_query
				query="update specs_rangs set nomen_es='Infra-orden' where id=22" && f_query
				query="update specs_rangs set nomen_es='Parv-orden' where id=23" && f_query
				query="update specs_rangs set nomen_es='Sección' where id=24" && f_query
				query="update specs_rangs set nomen_es='Sub-sección' where id=25" && f_query
				query="update specs_rangs set nomen_es='Super-familia' where id=26" && f_query
				query="update specs_rangs set nomen_es='Familia' where id=27" && f_query
				query="update specs_rangs set nomen_es='Sub-familia' where id=28" && f_query
				query="update specs_rangs set nomen_es='Super-tribu' where id=29" && f_query
				query="update specs_rangs set nomen_es='Tribu' where id=30" && f_query
				query="update specs_rangs set nomen_es='Sub-tribu' where id=31" && f_query
				query="update specs_rangs set nomen_es='Género' where id=32" && f_query
				query="update specs_rangs set nomen_es='Sub-género' where id=33" && f_query
				query="update specs_rangs set nomen_es='Sección' where id=34" && f_query
				query="update specs_rangs set nomen_es='Sub-sección' where id=35" && f_query
				query="update specs_rangs set nomen_es='Series' where id=36" && f_query
				query="update specs_rangs set nomen_es='Sub-series' where id=37" && f_query
				query="update specs_rangs set nomen_es='Aggregatos' where id=38" && f_query
				query="update specs_rangs set nomen_es='Especies' where id=39" && f_query
				query="update specs_rangs set nomen_es='Semi-especies' where id=40" && f_query
				query="update specs_rangs set nomen_es='Micro-especies' where id=41" && f_query
				query="update specs_rangs set nomen_es='Sub-especies' where id=42" && f_query
				query="update specs_rangs set nomen_es='Natio' where id=43" && f_query
				query="update specs_rangs set nomen_es='Variedad' where id=44" && f_query
				query="update specs_rangs set nomen_es='Sub-variedad' where id=45" && f_query
				query="update specs_rangs set nomen_es='Forma' where id=46" && f_query
				query="update specs_rangs set nomen_es='Sub-forma' where id=47" && f_query
				query="update specs_rangs set nomen_es='Forma-especies' where id=48" && f_query
				query="update specs_rangs set nomen_es='Linea' where id=49" && f_query
				query="update specs_rangs set nomen_es='Clona' where id=50" && f_query
				query="update specs_rangs set nomen_es='Raza' where id=51" && f_query
				query="update specs_rangs set nomen_es='Cultivar' where id=52" && f_query
				query="update specs_rangs set nomen_es='Morphia' where id=53" && f_query
				query="update specs_rangs set nomen_es='Abberatio' where id=54" && f_query
				query="insert into specs_rangs (refid,code,nomen_fr,nomen_en,nomen_es) values (1000,'unk','inconnu','unknown','desconocido');" && f_query
			else
				txtid=49 && f_gettext && echo "$red$thistxt$def"
			fi
		fi
	}
	function f_taxonfill { # créer et remplir les tables taxons_* - ca a pris 3 heures sur mon pc
		txtid=50 && f_gettext && echo "$blu$thistxt$def" # No need to recode, this one is already in utf-8
		i=0
#		sed -e 's/^\|$/"/g' -e 's/\t/"\t"/g' -e "s/[']//g" < TAXREFv14.txt > TAXREFv14_2.txt
		reignlist="Animalia Bacteria Chromista Fungi Plantae Protozoa Archaea"
		txtid=51 && f_gettext && echo "$gre$thistxt$def: "
		txtid=52 && f_gettext && echo "$gre$thistxt$def"
		read -r -p "$yel $reignlist $def" reignanswer
		if [[ "$reignanswer" != "" ]]; then reignlist="$reignanswer"; fi
		txtid=53 && f_gettext && echo "$mag$thistxt$def"
		while IFS=$'\t' read -r -a myArray; do
			((i++)) && if [[ "$i" == *00000 ]]; then txtid=54 && f_gettext && echo "$i $red$thistxt$def"; fi
			regne=${myArray[0]//\"}
			if [[ "$regne" != "" ]] && [[ "$regne" != "REGNE" ]] && [[ "$(echo "$reignlist"|grep "$regne")" != "" ]]; then
				nomen=${myArray[16]//\"} && nomvalide=${myArray[18]//\"}
				if [[ "$nomen" != "" ]] && [[ "$nomen" = "$nomvalide" ]]; then
					ttable="taxon_$regne" && varname="check_$ttable"
					if [[ "${!varname}" != "1" ]]; then
						if ! f_table_exists; then #si la tablen'existe pason la crée
							f_createtaxontable
						fi
					fi
					if ! f_record_exists ; then
						tempo="${myArray[19]%%,*}";nomen_fr="'${tempo[*]//\"}'"
						if [[ "$nomen_fr" = "''" ]]; then nomen_fr="NULL"; fi
						tempo="${myArray[20]%%,*}";nomen_en="'${tempo[*]//\"}'"
						if [[ "$nomen_en" = "''" ]]; then nomen_en="NULL"; fi
						if [[ "$mmode" = "human" ]]; then
							if [[ "$nomen_fr" = "NULL" ]] && [[ "$nomen_en" = "NULL" ]]; then continue 1; fi
						fi
						habitat=${myArray[21]//\"} && if [[ "$habitat" = "" ]]; then habitat='9'; fi
						if [[ "$regne" = "Plantae" ]] ; then f_randgreen; else color="DDDDDD"; fi
#						query="insert into $ttable (nomen,nomen_fr,nomen_en,nomen_es,color,phylum,classe,ordre,famille,sousfamille,tribu,group1inpm,group2inpm,rang,habitat,statutfr,source) values ('$nomen','$nomen_fr','$nomen_en','$nomen_es','$color','${myArray[1]//\"}','${myArray[2]//\"}','${myArray[3]//\"}','${myArray[4]//\"}','${myArray[5]//\"}','${myArray[6]//\"}','${myArray[7]//\"}','${myArray[8]//\"}','${myArray[13]//\"}','$habitat','${myArray[22]//\"}','$ssource');" && f_query
						query="insert into $ttable (nomen,nomen_fr,nomen_en,nomen_es,color,phylum,classe,ordre,famille,sousfamille,tribu,group1inpm,group2inpm,rang,habitat,statutfr,source) values ('$nomen','$nomen_fr','$nomen_en','$nomen_es','$color','${myArray[1]//\"}','${myArray[2]//\"}','${myArray[3]//\"}','${myArray[4]//\"}','${myArray[5]//\"}','${myArray[6]//\"}','${myArray[7]//\"}','${myArray[8]//\"}','${myArray[13]//\"}','$habitat','${myArray[22]//\"}','$ssource');" && f_query
					fi
				fi
			fi
	        done < TAXREFv14_2.txt
#		rm TAXREFv14_2.txt
	}
	f_create_taxref_habitats
	f_create_taxref_rangs
	f_taxonfill # requires habitats and rangs to exist (secondary key constraints in db)
}

function f_delete_taxreffr { # Effacer toutes les tables de specs et de taxons dépendant de taxreffr
	query="SET FOREIGN_KEY_CHECKS=0;drop tables taxon_Animalia, taxon_Archaea, taxon_Bacteria, taxon_Chromista, taxon_Fungi, taxon_Plantae, taxon_Protozoa;drop tables specs_habitats, specs_rangs;SET FOREIGN_KEY_CHECKS=1;"
	f_query
}
function f_merge_canadensys { # Procedure to merge plant taxons from Canada. Source: https://data.canadensys.net/ipt/resource?r=vascan
	# Steps:
	# 1. We create tempmerge table and write the taxons in it
	# 2. We add vernicular names to tempmerge table
	# 3. We merge tempmerge in taxon_Plantae, by
	#       - If nomen doesn't exist: add new record
	#       - If nomen exists, fill missing languages if any (nomen_fr nomen_en)
	ssource=canadensys
	nomen_es="NULL" # not available in this db
	regne=Plantae # static, as this db has only plants
	habitat="9" # unknown
	ttable="tempmerge" # temp table name
	function f_docreatetempmergetable {
		query="CREATE TABLE $ttable (id INT AUTO_INCREMENT PRIMARY KEY, orid INT, orparid INT, nomen VARCHAR(100) unique NOT NULL, nomen_en VARCHAR(100), nomen_fr VARCHAR(100), nomen_es VARCHAR(100), color VARCHAR(10), class VARCHAR(50), oorder VARCHAR(50), family VARCHAR(50), genus VARCHAR(50), subgenus VARCHAR(50), specepit VARCHAR(150), infraspecepit VARCHAR(150), taxonrank VARCHAR(50), source VARCHAR(50)) ENGINE = InnoDB;" && f_query
	}
	function f_createtempmergetable {
		if f_table_exists; then
			aanswer="" && read -r -p "$red La table temporaire existe - dites $def y $red pour la réinitialiser? $def " aanswer
			if [[ "$aanswer" = "y" ]]; then
				query="drop table $ttable;" && f_query && f_docreatetempmergetable
			fi
		else f_docreatetempmergetable
		fi
		#wget -q -O canadensys.zip https://data.canadensys.net/ipt/archive.do?r=vascan&v=37.9
		#unzip canadensys.zip
		ttable="tempmerge" # temp table name
	}
	function f_filltempdb {
		echo "$yel 1. Creating temporary taxon db for $ssource $def"
		sed -e 's/^\|$/"/g' -e 's/\t/"\t"/g' -e "s/[']//g" < taxon.txt > taxon2.txt
		while IFS=$'\t' read -r -a myArray; do
			acceptname=${myArray[19]//\"}
			if [[ "$acceptname" = "accepted" ]]; then # || [[ "$acceptname" = "synonym" ]]; then  # We only want accepted names
				orid=${myArray[0]//\"}
				orparid=${myArray[3]//\"}
				nomen=${myArray[6]//\"}
				class=${myArray[10]//\"}
				order=${myArray[11]//\"}
				family=${myArray[12]//\"}
				genus=${myArray[13]//\"}
				subgenus=${myArray[14]//\"}
				specepit=${myArray[15]//\"}
				infraspecepit=${myArray[16]//\"}
				taxonrank=${myArray[17]//\"}
				f_randgreen
				query="insert into $ttable (orid,orparid,nomen,color,class,oorder,family,genus,subgenus,specepit,infraspecepit,taxonrank,source) VALUES ($orid,$orparid,'$nomen','$color','$class','$order','$family','$genus','$subgenus','$specepit','$infraspecepit','$taxonrank','$ssource')" && f_query
			fi
		done < taxon2.txt
	}
	function f_filtempdblang {
		echo "$yel 2. Adding vernacular names in temporary taxon db.$def"
		sed -e 's/^\|$/"/g' -e 's/\t/"\t"/g' -e "s/[']//g" < vernacularname.txt > vernacularname2.txt
		while IFS=$'\t' read -r -a myArray; do
			orid=${myArray[0]//\"}
			name=${myArray[1]//\"}
			lang=${myArray[3]//\"}
			pref=${myArray[4]//\"}
			if [[ "$pref" = "true" ]]; then  # We only want data where isPreferredName is true
				if [[ "$lang" = "FR" ]] || [[ "$lang" = "EN" ]]; then # check for right lang
					lang=("$(echo "$lang" | tr '[:upper:]' '[:lower:]')")
					query="update $ttable set nomen_${lang[0]}='$name' where orid='$orid' and source='$ssource'" && f_query
				fi
			fi
		done < vernacularname2.txt
		echo "$yel 3. Merging temp db in main db.$def"
		query="select * from tempmerge" && f_query
	}
	function f_mergetempdb {
		query="select * from $ttable" && f_query
		for lline in "${sqlanswer[@]}";do
			echo "$lline" | while IFS=$'\t' read -r -a linearray; do
				if [[ "$mmode" = "human" ]]; then
					if [[ "${linearray[4]}" = "NULL" ]] && [[ "${linearray[5]}" = "NULL" ]] && [[ "${linearray[6]}" = "NULL" ]]; then continue 1; fi
				fi
				shortname=$(echo "${linearray[3]}"|sed -e 's/(L.).*//' -e 's/L., 1753.*//' -e 's/Linnaeus.*//')
				query="select id from taxon_$regne where nomen like '$shortname%' limit 1" && f_query && id="${sqlanswer[0]}"
				if [[ "$id" = "" ]]; then
					rrank=""
					query="select code from specs_rangs where nomen_en='${linearray[13]//\"}';" && f_query2
					rrank="${results[0]}" && if [[ "$rrank" = "" ]]; then rrank="unk"; fi
					nomen_fr="'${linearray[5]}'"
					if [[ "$nomen_fr" = "'NULL'" ]]; then nomen_fr="NULL";
					else
						query="select id from taxon_Plantae where nomen_fr=$nomen_fr;" && f_query2
						if [[ "${results[0]}" != "" ]]; then continue 1; fi
					fi
					nomen_en="'${linearray[4]}'"
					if [[ "$nomen_en" = "'NULL'" ]]; then nomen_en="NULL";
					else
						query="select id from taxon_Plantae where nomen_en=$nomen_en;" && f_query2
						if [[ "${results[0]}" != "" ]]; then continue 1; fi
					fi
					query="insert into taxon_Plantae (nomen,nomen_fr,nomen_en,nomen_es,color,phylum,classe,ordre,famille,sousfamille,tribu,group1inpm,group2inpm,rang,habitat,statutfr,source) values ('${linearray[3]}',$nomen_fr,$nomen_en,NULL,'${linearray[7]}',NULL,'${linearray[8]}','${linearray[9]}','${linearray[10]}',NULL,NULL,NULL,NULL,'$rrank','$habitat',NULL,'$ssource');" && f_query
				else
					new_nomen_fr="NULL"; old_nomen_fr=""
					query="select nomen_fr from taxon_$regne where id=$id limit 1" && f_query && old_nomen_fr="${sqlanswer[0]}"
					if [[ "$old_nomen_fr" = "NULL" ]]; then
						if [[ "${linearray[5]}" != "NULL" ]]; then
							new_nomen_fr="${linearray[5]}"
							query="update taxon_Plantae set nomen_fr='$new_nomen_fr' where id=$id" && f_query
						fi
					fi
					new_nomen_en="NULL"; old_nomen_en=""
					query="select nomen_en from taxon_$regne where id=$id limit 1" && f_query && old_nomen_en="${sqlanswer[0]}"
					if [[ "$old_nomen_en" = "NULL" ]]; then
						if [[ "${linearray[4]}" != "NULL" ]]; then
							new_nomen_en="${linearray[4]}"
							query="update taxon_Plantae set nomen_en='$new_nomen_en' where id=$id" && f_query
						fi
					fi
				fi
			done
		done
	}
	f_filltempdb
	f_filtempdblang
	f_mergetempdb
}

function m_taxreffr {
	m_taxreffr=("Taxreffr" "f_create_taxreffr" "f_delete_taxreffr")
	f_menu "${m_taxreffr[@]}"
}
function m_canadensys {
	m_canadensys=("Canadensys" "f_merge_canadensys")
	f_menu "${m_canadensys[@]}"
}
function m_main { # Main Menu
	m_main=("TaxonDB" "m_taxreffr" "m_canadensys" "f_summary" "f_backup_db")
	f_menu "${m_main[@]}"
}

#f_create_taxreffr
#f_merge_canadensys

#entry point
m_main && echo "$blu bye $def" && exit
